package com.chen.netty.netty.server;


import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Auther: chen
 * @Date:
 * @Description:
 */
public class ServerHandle extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf buffer = (ByteBuf) msg;
        byte [] bytes = new byte[buffer.readableBytes()];
        buffer.readBytes(bytes);
        String body = new String(bytes,"UTF-8");
        System.out.println("客户端发送的数据->"+ body);
        String res = "现在是北京时间:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        ByteBuf byteBuf = Unpooled.copiedBuffer(res.getBytes());
        ctx.writeAndFlush(byteBuf);
    }


    //读取完成后处理方法
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        System.out.println("EchoServerHandler.channelReadComplete");
        //ctx.flush();
    }

    //异常捕获处理方法
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
