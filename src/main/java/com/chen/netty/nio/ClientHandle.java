package com.chen.netty.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * @Auther: chen
 * @Date:
 * @Description:
 */
public class ClientHandle implements Runnable {
    private int port;

    private volatile boolean stop;

    private Selector selector;
    private String ip;
    private SocketChannel socketChannel;

    public ClientHandle(String ip, int port) throws IOException {
        this.ip = ip;
        this.port = port;
        selector = Selector.open();
        socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);
    }

    @Override
    public void run() {
        try {
            //建立连接
            doConnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (!stop) {
            try {
                selector.select(1000);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                iterator.remove();
                try {
                    handleKey(key);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        // 多路复用器关闭后，所有注册在上面的Channel和Pipe等资源都会被自动去注册并关闭，所以不需要重复释放资源
        if (selector != null) {
            try {
                selector.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 处理key
     * @param key
     * @throws IOException
     */
    private void handleKey(SelectionKey key) throws IOException {
        /**
         * 验证key是否有效
         */
        if (key.isValid()) {
            //检查连接是否成功
            SocketChannel sc = (SocketChannel) key.channel();
            //key 连接状态
            if (key.isConnectable()) {
                //检查连接完成
                if (sc.finishConnect()) {
                    //完成，注册读事件
                    sc.register(selector, SelectionKey.OP_READ);
                    doWrite(sc);
                } else {
                    System.exit(1);
                }
            }
            //key 读取状态
            if (key.isReadable()) {
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                int read = sc.read(buffer);
                if (read > 0) {
                    //buffer长度是1024 ，假如但是实际存储的位置10，那我要把0-10的数据读取出来，上线limit设置为10
                    buffer.flip();
                    byte[] bytes = new byte[buffer.remaining()];
                    buffer.get(bytes);
                    String body = new String(bytes, "UTF-8");
                    System.out.println("客户端：接收到服务数据,内容 " + body);
                    this.stop = true;
                } else if (read < 0) {
                    key.cancel();
                    sc.close();
                } else {

                }
            }
        }
    }

    /**
     * 建立连接
     * @throws IOException
     */
    private void doConnect() throws IOException {
        //建立连接成功，则注册读取事件
        if (socketChannel.connect(new InetSocketAddress(ip, port))) {
            socketChannel.register(selector, SelectionKey.OP_READ);
            //成功了，发送数据给服务器
            doWrite(socketChannel);
        } else {
            //注册失败，继续注册连接事件
            socketChannel.register(selector, SelectionKey.OP_CONNECT);
        }
    }

    /**
     * 发送数据
     * @param socketChannel
     * @throws IOException
     */
    private void doWrite(SocketChannel socketChannel) throws IOException {
        byte[] bytes = "我是客户端".getBytes();
        ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
        buffer.put(bytes);
        buffer.flip();
        socketChannel.write(buffer);
        if (!buffer.hasRemaining()) {
            System.out.println("客户端发送完毕！");
        }

    }
}
