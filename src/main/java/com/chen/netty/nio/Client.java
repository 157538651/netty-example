package com.chen.netty.nio;

import java.io.IOException;

/**
 * @Auther: chen
 * @Date:
 * @Description:
 */
public class Client {
    public static void main(String[] args) {
        ClientHandle clientHandle = null;
        try {
            clientHandle = new ClientHandle("127.0.0.1",9092);
            new Thread(clientHandle,"CLIENT-CHEN-THREAD").start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
