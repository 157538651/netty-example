package com.chen.netty.nio;

import java.io.IOException;

/**
 * @Auther: chen
 * @Date:
 * @Description:
 */
public class Server {
    public static void main(String[] args){
        ServerHandle serverHandle = null;
        try {
            serverHandle = new ServerHandle(9092);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //启动多路复用器ServerHandle来处理请求
        new Thread(serverHandle,"SERVER-CHEN-THREAD").start();
    }
}
