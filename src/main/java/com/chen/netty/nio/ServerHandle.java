package com.chen.netty.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 * @Auther: chen
 * @Date:
 * @Description:
 */
public class ServerHandle implements Runnable {
    private int port;

    private volatile boolean stop;

    private Selector selector;
    private ServerSocketChannel serverSocketChannel;

    /**
     * 初始化多路复用器
     *
     * @param port
     * @throws IOException
     */
    public ServerHandle(int port) throws IOException {
        this.port = port;
        selector = Selector.open();
        serverSocketChannel = ServerSocketChannel.open();
        //设置非阻塞模式
        serverSocketChannel.configureBlocking(false);
        //监听端口
        serverSocketChannel.bind(new InetSocketAddress(port));
        //注册ACCEPT 事件，表示关注客户端的注册事件
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        System.out.println("服务器初始化完毕");
    }

    public void stop() {
        this.stop = true;
    }

    @Override
    public void run() {
        while (!stop) {
            try {
                //每隔1s检查一次
                selector.select(1000);
                //获取到已经准备的keys，就是已经准备好的事件
                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = selectionKeys.iterator();
                while (iterator.hasNext()) {
                    //获取单个事件
                    SelectionKey key = iterator.next();
                    //处理key
                    try {
                        handleKey(key);
                    } catch (Exception ex) {
                        //出异常了，取消key
                        if (null != key) {
                            key.cancel();
                            if (key.channel() != null) {
                                try {
                                    key.channel().close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }finally {
                        //处理后，把key移除掉
                        iterator.remove();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (null != selector) {
            try {
                //多路复用器关闭后，所有注册在复用器上面的channel和pipe都会自动关闭
                selector.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 处理key
     * @param key
     * @throws IOException
     */
    private void handleKey(SelectionKey key) throws IOException {
        //判断key是否有效
        if (key.isValid()) {
            //key 连接状态
            if (key.isAcceptable()) {
                //客户端的连接请求
                ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
                SocketChannel sc = ssc.accept();
                sc.configureBlocking(false);
                //连接上了，注册OP_READ 事件
                sc.register(selector, SelectionKey.OP_READ);
                System.out.println("有客户端来连接了，帮他注册读事件");
            }
            //key 是否可读状态
            if (key.isReadable()) {
                SocketChannel sc = (SocketChannel) key.channel();
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                //读取数据
                int readData = sc.read(buffer);
                // >0 读取到了数据
                if (readData > 0) {
                    buffer.flip();
                    byte[] bytes = new byte[buffer.remaining()];
                    buffer.get(bytes);
                    String body = new String(bytes, "UTF-8");
                    System.out.println("服务器: 接收到客户端的数据，内容：" + body);
                    //发送数据给客户端
                    doWrite(sc, "现在是北京时间:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                    //将key标记为可读
                    key.interestOps(SelectionKey.OP_READ);
                } else if (readData < 0) {
                    //链路关闭了，则取消key，关闭sc
                    key.cancel();
                    sc.close();
                } else {
                   // readData==0
                    // 没有读取到数据
                }
            }
        }
    }

    /**
     * 发送数据
     * @param socketChannel
     * @param hello
     * @throws IOException
     */
    private void doWrite(SocketChannel socketChannel, String hello) throws IOException {
        if (hello != null) {
            byte[] bytes = hello.getBytes();
            ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
            buffer.put(bytes);
            //当前缓冲区是读状态，通过flip改变成写状态
            buffer.flip();
            socketChannel.write(buffer);
        }

    }
}
