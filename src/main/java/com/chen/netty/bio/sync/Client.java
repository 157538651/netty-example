package com.chen.netty.bio.sync;

import java.io.*;
import java.net.Socket;

/**
 * @Auther: chen
 * @Date:
 * @Description:
 */
public class Client {
    public static void main(String[] args){
        BufferedReader in = null;
        PrintWriter out = null;
        Socket socket = null;
        try {
             socket = new Socket("127.0.0.1",9091);
            InputStream inputStream = socket.getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream);

            out = new PrintWriter(socket.getOutputStream(), true);
            out.println("我是客户端");
            System.out.println("向客户端发送消息");

            in = new BufferedReader(reader);
            System.out.println(in.readLine());

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(null!=in){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(null!=out){
                out.close();
            }
            if(null!=socket){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
