package com.chen.netty.bio.sync;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Auther: chen
 * @Date:
 * @Description:
 */
public class Server {
    public static void main(String[] args) {
        ServerSocket serverSocket = null;
        Socket socket = null;
        try {
            serverSocket = new ServerSocket(9091);
            while (true) {
                socket = serverSocket.accept();
                System.out.println("客户端来连接了");
                new Thread(new ServerHandle(socket)).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(null!=socket){
                try {
                    System.out.println("处理完，关闭连接");
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
