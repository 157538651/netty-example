package com.chen.netty.bio.async;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Auther:
 * @Date: chen
 * @Description:
 */
public class ThreadPoolServer {
    ExecutorService executorService = null;

    public ThreadPoolServer(int corePoolSize, int maximumPoolSize, int queueSize) {
        executorService = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
                100L, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(queueSize));
    }

    public void execute(Runnable task){
        executorService.execute(task);
    }
}
