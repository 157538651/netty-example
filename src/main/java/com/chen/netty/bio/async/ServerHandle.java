package com.chen.netty.bio.async;

import java.io.*;
import java.net.Socket;

/**
 * @Auther: chen
 * @Date:
 * @Description:
 */
public class ServerHandle implements Runnable{
    Socket socket;

    public ServerHandle(Socket accept) {
        this.socket = accept;
    }

    @Override
    public void run() {
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            InputStream inputStream = socket.getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream);
            in = new BufferedReader(reader);
            out = new PrintWriter(socket.getOutputStream(), true);
            while (true){
                String body = in.readLine();
                if(null==body){
                    break;
                }
                System.out.println("接收到服务器发送来的数据："+body);
                out.println("服务器收到了");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(null!=in){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(null!=out){
                out.close();
            }
        }
    }
}
